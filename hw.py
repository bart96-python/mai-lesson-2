import random


def createSecretNumber():
	secretNumber = random.randint(0, float('1e4') - 1)
	return list('{:04d}'.format(secretNumber))


def isValid(msg: str):
	if msg == 'q':
		exit('Game over :(')
	if not msg.isdigit():
		return print('Ошибка: Нужно ввести целое число')
	if len(msg) != 4:
		return print('Ошибка: Число должно быть 4-значным')
	else:
		return True


def checkCorrect(attemptCount: list, inputValue: str, secretNumber: list):
	output = [None] * 4
	secretNumberClone = secretNumber.copy()

	for index in range(len(output)):
		if inputValue[index] == secretNumber[index]:
			output[index] = 'B'
			secretNumberClone[index] = None

	for index in range(len(output)):
		if output[index] == None and inputValue[index] in secretNumberClone:
			output[index] = 'K'

	print(showResult(attemptCount, inputValue, ''.join(map(str, filter(None, output)))))


def showResult(attemptCount: list, inputValue: str, output: str):
	attemptCount[0] += 1
	if output == 'B' * 4:
		exit(f'Победа! Вы угадали {inputValue} за {attemptCount[0]} раз')
	else:
		return f'{attemptCount[0]}. Вы ввели: {inputValue} Результат: {output or "-"}'


def main():
	attemptCount = [0]
	secretNumber = createSecretNumber()

	while True:
		inputValue = input('Введите 4-значное число или "q" для выхода: ')
		if isValid(inputValue):
			checkCorrect(attemptCount, inputValue, secretNumber)


main()
